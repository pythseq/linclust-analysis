predictGene(){
if [ ! -f prot.$1.fasta ]; then
   prodigal.linux -i $1 -a prot.$1.fasta > /dev/null 2> /dev/null
fi
}

N=1
(
for fasta in $(ls *.fasta); do
   ((i=i%N)); ((i++==0)) && wait
   predictGene "$fasta" &
done
)
