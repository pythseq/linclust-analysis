#!/bin/bash
# Clustering workflow script
checkReturnCode () {
        [ $? -ne 0 ] && echo "$1" && exit 1;
}
notExists () {
        [ ! -f "$1" ]
}
#pre processing
#[ -z "$MMDIR" ] && echo "Please set the environment variable \$MMDIR to your MMSEQS installation directory." && exit 1;
# check amount of input variables
[ "$#" -ne 3 ] && echo "Please provide <sequenceDB> <outDB> <tmp>" && exit 1;
# check if files exists
[ ! -f "$1" ] &&  echo "$1 not found!" && exit 1;
[   -f "$2" ] &&  echo "$2 exists already!" && exit 1;
[ ! -d "$3" ] &&  echo "tmp directory $3 not found!" && exit 1;

export OMP_PROC_BIND=TRUE
MMSEQS=mmseqs
KMERMATCHER_PAR="-c 0.0 --cov-mode 1 -c 0.8 --alph-size 14 --kmer-per-seq 20"
HAMMING_PAR="--rescore-mode 0 -c 0.0 --cov-mode 1 -c 0.99 --min-seq-id 0.95"
UNGAPPED_ALN_PAR="--rescore-mode 1 --filter-hits -c 0.0 --cov-mode 1 -c 0.8 --min-seq-id 0.5"
ALIGNMENT_PAR="--alignment-mode 2 -e 0.001 -c 0.0 --min-seq-id 0.5 --cov-mode 1 -c 0.8"
INPUT="$1"
# 1. Finding exact $k$-mer matches.
notExists "$3/pref"          && $MMSEQS kmermatcher "$INPUT" "$3/pref" ${KMERMATCHER_PAR}                    && checkReturnCode "Kmer matching step died"
# 2. Hamming distance pre-clustering
notExists "$3/pref_rescore1" && $MMSEQS rescorediagonal $INPUT $INPUT "$3/pref" "$3/pref_rescore1" ${HAMMING_PAR} && checkReturnCode "Rescore with hamming distance step died"
notExists "$3/pre_clust"     && $MMSEQS clust  $INPUT "$3/pref_rescore1" "$3/pre_clust" --cluster-mode 2            && checkReturnCode "Pre-clustering step died"
awk '{ print $1 }' "$3/pre_clust.index" > "$3/order_redundancy"
notExists "$3/input_step_redundancy" && $MMSEQS createsubdb "$3/order_redundancy" $INPUT "$3/input_step_redundancy"     && checkReturnCode "Createsubdb step died"
notExists "$3/pref_filter1"  && $MMSEQS createsubdb "$3/order_redundancy" "$3/pref" "$3/pref_filter1"                    && checkReturnCode "Createsubdb step died"
notExists "$3/pref_filter2"  && $MMSEQS filterdb "$3/pref_filter1" "$3/pref_filter2" --filter-file "$3/order_redundancy" && checkReturnCode "Filterdb step died"
INPUT="$3/input_step_redundancy"
# 3. Ungapped alignment filtering
notExists "$3/pref_rescore2" && $MMSEQS rescorediagonal "$INPUT" "$INPUT" "$3/pref_filter2" "$3/pref_rescore2" ${UNGAPPED_ALN_PAR} && checkReturnCode "Ungapped alignment step died"
# 4. Local gapped sequence alignment.
notExists "$3/aln"           && $RUNNER $MMSEQS align "$INPUT" "$INPUT" "$3/pref_rescore2" "$3/aln"  ${ALIGNMENT_PAR}            && checkReturnCode "Alignment step died"
# 5. Clustering using greedy set cover.
notExists "$3/clust"         && $MMSEQS clust "$INPUT" "$3/aln" "$3/clust"  && checkReturnCode "Clustering step died"
notExists "$3/clust.tsv"         && $MMSEQS createtsv "$1" "$1" "$3/clust" "$3/clust.tsv"  && checkReturnCode "Clustering tsv step died"
awk '{ print $1 }' "$3/clust.index" > "$3/order_redundancy2"
$MMSEQS createsubdb "$3/order_redundancy2" $1 "$3/seq_clust"

$MMSEQS summarizeheaders $1 $1 $3/clust $3/CLUST_SUM_HEADER --header-type 2 --summary-prefix mc
$MMSEQS mergedbs $3/clust $3/metaclust50 $3/CLUST_SUM_HEADER $1/seq_clust --prefixes ">"
$MMSEQS convert2fasta $INPUT "$3/metaclust95.fasta"
sed -i 's/\x0//g' "$3/metaclust50"
mv $3/metaclust50 $3/metaclust50.fasta
