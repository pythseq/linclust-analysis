mkdir -p linclust90_32/
time mmseqs linclust uniprot_db_32 uniprot_db_32_linclust_clu90 linclust90_32/ --cov-mode 1 -c 0.9  --min-seq-id 0.9
mkdir -p linclust90_16/
time mmseqs linclust uniprot_db_16 uniprot_db_16_linclust_clu90 linclust90_16/ --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p linclust90_8/
time mmseqs linclust uniprot_db_8 uniprot_db_8_linclust_clu90 linclust90_8/  --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p linclust90_4/
time mmseqs linclust uniprot_db_4 uniprot_db_4_linclust_clu90 linclust90_4/  --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p linclust90_2/
time mmseqs linclust uniprot_db_2 uniprot_db_2_linclust_clu90 linclust90_2/  --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p linclust90_1/
time mmseqs linclust uniprot_db uniprot_db_linclust_clu90 linclust90_1/      --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p linclust90_halfx2/
time mmseqs linclust uniprot_db_halfx2 uniprot_db_linclust_halfx2_clu90 linclust90_halfx2/ --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p linclust90_x2/
time mmseqs linclust uniprot_db_x2 uniprot_db_linclust_x2_clu90 linclust90_x2/ --cov-mode 1 -c 0.9 --min-seq-id 0.9
