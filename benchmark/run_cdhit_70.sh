#!/bin/bash
time cd-hit -T 16 -n 4 -M 0 -c 0.7 -i uniprot_db_32.fasta -o cd-hit_2016_03_70_32
time cd-hit -T 16 -n 4 -M 0 -c 0.7 -i uniprot_db_16.fasta -o cd-hit_2016_03_70_16
time cd-hit -T 16 -n 4 -M 0 -c 0.7 -i uniprot_db_8.fasta -o cd-hit_2016_03_70_8  
time cd-hit -T 16 -n 4 -M 0 -c 0.7 -i uniprot_db_4.fasta -o cd-hit_2016_03_70_4  
time cd-hit -T 16 -n 4 -M 0 -c 0.7 -i uniprot_db_2.fasta -o cd-hit_2016_03_70_2
