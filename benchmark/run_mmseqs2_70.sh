mkdir -p mmseqs70_32/
time mmseqs cluster uniprot_db_32 uniprot_db_32_mmseqs_clu70 mmseqs70_32/ --cov-mode 1 -c 0.9  --min-seq-id 0.7
mkdir -p mmseqs70_16/
time mmseqs cluster uniprot_db_16 uniprot_db_16_mmseqs_clu70 mmseqs70_16/ --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p mmseqs70_8/
time mmseqs cluster uniprot_db_8 uniprot_db_8_mmseqs_clu70 mmseqs70_8/  --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p mmseqs70_4/
time mmseqs cluster uniprot_db_4 uniprot_db_4_mmseqs_clu70 mmseqs70_4/  --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p mmseqs70_2/
time mmseqs cluster uniprot_db_2 uniprot_db_2_mmseqs_clu70 mmseqs70_2/  --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p mmseqs70_1/
time mmseqs cluster uniprot_db uniprot_db_mmseqs_clu70 mmseqs70_1/      --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p mmseqs70_halfx2/
time mmseqs cluster uniprot_db_halfx2 uniprot_db_mmseqs_halfx2_clu70 mmseqs70_halfx2/ --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p mmseqs70_x2/
time mmseqs cluster uniprot_db_x2 uniprot_db_mmseqs_x2_clu70 mmseqs70_x2/ --cov-mode 1 -c 0.9 --min-seq-id 0.7
