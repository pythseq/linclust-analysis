mkdir -p linclust70_32/
time mmseqs linclust uniprot_db_32 uniprot_db_32_linclust_clu70 linclust70_32/ --cov-mode 1 -c 0.9  --min-seq-id 0.7
mkdir -p linclust70_16/
time mmseqs linclust uniprot_db_16 uniprot_db_16_linclust_clu70 linclust70_16/ --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p linclust70_8/
time mmseqs linclust uniprot_db_8 uniprot_db_8_linclust_clu70 linclust70_8/  --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p linclust70_4/
time mmseqs linclust uniprot_db_4 uniprot_db_4_linclust_clu70 linclust70_4/  --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p linclust70_2/
time mmseqs linclust uniprot_db_2 uniprot_db_2_linclust_clu70 linclust70_2/  --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p linclust70_1/
time mmseqs linclust uniprot_db uniprot_db_linclust_clu70 linclust70_1/      --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p linclust70_halfx2/
time mmseqs linclust uniprot_db_halfx2 uniprot_db_linclust_halfx2_clu70 linclust70_halfx2/ --cov-mode 1 -c 0.9 --min-seq-id 0.7
mkdir -p linclust70_x2/
time mmseqs linclust uniprot_db_x2 uniprot_db_linclust_x2_clu70 linclust70_x2/ --cov-mode 1 -c 0.9 --min-seq-id 0.7
