mkdir -p mmseqs90_32/
time mmseqs cluster uniprot_db_32 uniprot_db_32_mmseqs_clu90 mmseqs90_32/ --cov-mode 1 -c 0.9  --min-seq-id 0.9
mkdir -p mmseqs90_16/
time mmseqs cluster uniprot_db_16 uniprot_db_16_mmseqs_clu90 mmseqs90_16/ --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p mmseqs90_8/
time mmseqs cluster uniprot_db_8 uniprot_db_8_mmseqs_clu90 mmseqs90_8/  --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p mmseqs90_4/
time mmseqs cluster uniprot_db_4 uniprot_db_4_mmseqs_clu90 mmseqs90_4/  --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p mmseqs90_2/
time mmseqs cluster uniprot_db_2 uniprot_db_2_mmseqs_clu90 mmseqs90_2/  --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p mmseqs90_1/
time mmseqs cluster uniprot_db uniprot_db_mmseqs_clu90 mmseqs90_1/      --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p mmseqs90_halfx2/
time mmseqs cluster uniprot_db_halfx2 uniprot_db_mmseqs_halfx2_clu90 mmseqs90_halfx2/ --cov-mode 1 -c 0.9 --min-seq-id 0.9
mkdir -p mmseqs90_x2/
time mmseqs cluster uniprot_db_x2 uniprot_db_mmseqs_x2_clu90 mmseqs90_x2/ --cov-mode 1 -c 0.9 --min-seq-id 0.9
